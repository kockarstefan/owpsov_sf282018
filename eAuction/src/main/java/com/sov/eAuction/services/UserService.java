package com.sov.eAuction.services;

import java.util.List;

import com.sov.eAuction.model.User;

public interface UserService {

	User findOne(String username); 
	User findOne(String username, String password);
	List<User> findAll(); 
	User save(User user); 
	List<User> save(List<User> users); 
	User update(User user); 
	List<User> update(List<User> users);
	User delete(String username); 
	List<User> findByIme(String name);
	List<User> findByPrezime(String prezime);
	List<User> findByAdministrator(boolean administrator);
	List<User> findByUlogovan(boolean ulogovan);
}
