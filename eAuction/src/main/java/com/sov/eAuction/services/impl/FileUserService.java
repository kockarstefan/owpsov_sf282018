package com.sov.eAuction.services.impl;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.sov.eAuction.model.User;
import com.sov.eAuction.services.UserService;

@Service
@Primary
@Qualifier("userFiles")
public class FileUserService implements UserService {

	
	@Value("${users.pathToFile}")
	private String pathToFile;
	
	private Map<Long, User> readFromFile() {
		
		Map<Long, User> users = new HashMap<>();
    	Long nextId = 1L;
    	
    	try {
			Path path = Paths.get(pathToFile);
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				String[] tokens = line.split(";");
				Long id = Long.parseLong(tokens[0]);
				String name = tokens[1];
				String surname  = tokens[2];
				String username = tokens[3];
				String password = tokens[4];
				boolean admin = Boolean.valueOf(tokens[5]);
				boolean loggedIn = Boolean.valueOf(tokens[6]);

				users.put(Long.parseLong(tokens[0]),new User(id, name, surname, username, password, admin, loggedIn));
				if(nextId<id)
					nextId=id;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return users;
    	
	}
	
private Map<Long, User> saveToFile(Map<Long, User> users) {
    	
    	Map<Long, User> ret = new HashMap();
    	
    	try {
			Path path = Paths.get(pathToFile);
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = new ArrayList<String>();
			
			for (User user : users.values()) {
				String line =user.getId()+";"+user.getName()+";"+user.getSurname()+";"+user.getUsername()+";"+user.getPassword()+";"+ user.isAdmin()+";"+ user.isLoggedIn();
				lines.add(line);
				ret.put(user.getId(), user);
			}
			
			Files.write(path, lines, Charset.forName("UTF-8"));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return ret;
    }
    
    private Long nextId(Map<Long, User> users) {
    	Long nextId = 1L;
    	for (Long id : users.keySet()) {
    		if(nextId<id)
				nextId=id;
		}
    	return ++nextId;
    }
	
	
	
	@Override
	public User findOne(String username) {
		Map<Long, User> users = readFromFile();	
		return users.get(username);
	}

	@Override
	public User findOne(String username, String password) {
		Map<Long, User> users = readFromFile();	
		User user = findOne(username);
		if(user!=null && user.getPassword().equals(password))
			return user;
		return null;
	}

	@Override
	public List<User> findAll() {
		Map<Long, User> users = readFromFile();
		return new ArrayList<User>(users.values());
	}

	@Override
	public User save(User user) {
		Map<Long, User> users = readFromFile();
		Long nextId = nextId(users); 
		
		if(user.getId() == null) {
			user.setId(nextId++);
		}
		users.put(user.getId(), user);
		saveToFile(users);
		return user;
	}

	@Override
	public List<User> save(List<User> users) {
		Map<Long, User> Users = readFromFile();
		Long nextId = nextId(Users); 
		
		List<User> ret = new ArrayList<>();

		for (User u : users) {
			
			
			if (u.getId() == null) {
				u.setId(nextId++);
				
			}
			
			Users.put(u.getId(), u);
		}
		Users = saveToFile(Users);
		ret = new ArrayList<User>(Users.values());

		return ret;
	}

	@Override
	public User update(User user) {
		Map<Long, User> users = readFromFile();
		
		users.put(user.getId(), user);
		saveToFile(users);
		return user;
	}

	@Override
	public List<User> update(List<User> users) {
		Map<Long, User> Users = readFromFile();
		
		List<User> ret = new ArrayList<>();
		for (User u : users) {
			Users.put(u.getId(), u);
		}
		Users = saveToFile(Users);
		ret = new ArrayList<User>(Users.values());

		return ret;
	}

	@Override
	public User delete(String username) {
		Map<Long, User> users = readFromFile();
		
		if (!users.containsKey(username)) {
			throw new IllegalArgumentException("tried to remove non existing film");
		}
		
		User user = findOne(username);
		if (user != null) {
			users.remove(user);
		}
		saveToFile(users);
		return user;	
	}

	@Override
	public List<User> findByIme(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByPrezime(String prezime) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByAdministrator(boolean administrator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByUlogovan(boolean ulogovan) {
		// TODO Auto-generated method stub
		return null;
	}

}
