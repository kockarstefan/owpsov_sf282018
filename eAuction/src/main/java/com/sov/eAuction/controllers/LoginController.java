package com.sov.eAuction.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;

import com.sov.eAuction.model.User;
import com.sov.eAuction.services.UserService;



@Controller
@RequestMapping(value = "/login")
public class LoginController{
	
	public static final String USER_KEY = "user";
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	 
	
	@SuppressWarnings("unchecked")
	@GetMapping(value="/Login")
	public void getLogin(@RequestParam(required = false) String username, @RequestParam(required = false) String password, HttpSession session, HttpServletResponse response) throws IOException {		
		postLogin(username, password, session, response);
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping(value="/Login")
	public void postLogin(@RequestParam(required = false) String username, @RequestParam(required = false) String password, HttpSession session, HttpServletResponse response) throws IOException {	
		
//		response.setHeader("Cache-Control", "private, no-cache, no-store, must-revalidate"); // HTTP 1.1
//		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
//		response.setDateHeader("Expires", 0); // Proxies.
		
		User user = userService.findOne(username);	
		String message = "";
		if(user==null)
			message="Wrong username<br/>";
		else if (!user.getUsername().equals(password))
			message="Wrong username<br/>";
		
		if(!message.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" + 
//					"	<META HTTP-EQUIV=\"Cache-Control\" CONTENT=\"no-cache\">\r\n" + 
//					"	<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"> \r\n" + 
//					"	<META HTTP-EQUIV=\"Expires\" CONTENT=\"0\">\r\n" +
					"	<meta charset=\"UTF-8\">\r\n" + 
					"	<title>Sign in</title>\r\n" + 
					"</head>\r\n" + 
					"<body>\r\n");
			if(!message.equals(""))
				retVal.append(
					"	<div>"+message+"</div>\r\n");
			retVal.append(
					"	<form method=\"post\" action=\"login/Login\">\r\n" + 
					"		<table>\r\n" + 
					"			<caption>Sign in</caption>\r\n" + 
					"			<tr><th>Username:</th><td><input type=\"text\" value=\"\" name=\"username\" required/></td></tr>\r\n" + 
					"			<tr><th>Password:</th><td><input type=\"password\" value=\"\" name=\"password\" required/></td></tr>\r\n" + 
					"			<tr><th></th><td><input type=\"submit\" value=\"Login\" /></td>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" + 
					"	<br/>\r\n" + 
					"</body>\r\n" + 
					"</html>");
			
			out.write(retVal.toString());
			return;
		}
		
		if (user.isLoggedIn()==true)
			message="User already logged in<br/>";
		else if (session.getAttribute(LoginController.USER_KEY)!=null)
			message="User already logged in<br/>";
		
		if(!message.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" + 
//					"	<META HTTP-EQUIV=\"Cache-Control\" CONTENT=\"no-cache\">\r\n" + 
//					"	<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"> \r\n" + 
//					"	<META HTTP-EQUIV=\"Expires\" CONTENT=\"0\">\r\n" +
					"	<meta charset=\"UTF-8\">\r\n" + 
					"	<title>Sign uo</title>\r\n" +  
					"</head>\r\n" + 
					"<body>\r\n");
			if(!message.equals(""))
				retVal.append(
					"	<div>"+message+"</div>\r\n");
			retVal.append(
					"	<a href=\"index.html\">Povratak</a>\r\n" + 
					"	<br/>\r\n" + 
					"</body>\r\n" + 
					"</html>");
			
			out.write(retVal.toString());
			return;
		}
		
		user.setLoggedIn(true);
		session.setAttribute(LoginController.USER_KEY, user);
		
		response.sendRedirect(bURL+"user");
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping(value="/Logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {	
		
//		response.setHeader("Cache-Control", "private, no-cache, no-store, must-revalidate"); // HTTP 1.1
//		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
//		response.setDateHeader("Expires", 0); // Proxies.
		
		User user = (User) request.getSession().getAttribute(LoginController.USER_KEY);
		String message = "";
		if(user==null || user.isLoggedIn()==false )
			message="Nobody is logged in <br/>";
		
		if(!message.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" +
//					"	<META HTTP-EQUIV=\"Cache-Control\" CONTENT=\"no-cache\">\r\n" + 
//					"	<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"> \r\n" + 
//					"	<META HTTP-EQUIV=\"Expires\" CONTENT=\"0\">\r\n" +
					"	<meta charset=\"UTF-8\">\r\n" + 
					"	<title>Sign in</title>\r\n" + 
					"</head>\r\n" + 
					"<body>\r\n");
			if(!message.equals(""))
				retVal.append(
					"	<div>"+message+"</div>\r\n");
			retVal.append(
					"	<form method=\"post\" action=\"login/Login\">\r\n" + 
					"		<table>\r\n" + 
					"			<caption>Sign in </caption>\r\n" + 
					"			<tr><th>Username:</th><td><input type=\"text\" value=\"\" name=\"username\" required/></td></tr>\r\n" + 
					"			<tr><th>Password:</th><td><input type=\"password\" value=\"\" name=\"password\" required/></td></tr>\r\n" + 
					"			<tr><th></th><td><input type=\"submit\" value=\"Login\" /></td>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" + 
					"	<br/>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"login/Logout\">Logout</a></li>\r\n" + 
					"	</ul>" +
					"</body>\r\n" + 
					"</html>");
			
			out.write(retVal.toString());
			return;
		}
		
		user.setLoggedIn(false);
		
		request.getSession().removeAttribute(LoginController.USER_KEY);
		request.getSession().invalidate();
		response.sendRedirect(bURL+"login/Login");
	}
	
	
}
