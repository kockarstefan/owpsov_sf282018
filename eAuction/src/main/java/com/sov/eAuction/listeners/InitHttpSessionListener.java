package com.sov.eAuction.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

@Component
public class InitHttpSessionListener implements HttpSessionListener {


	public void sessionCreated(HttpSessionEvent arg0) {
		System.out.println("Initializing session...");

		HttpSession session  = arg0.getSession();
		System.out.println("user session id: "+session.getId());
//		session.setAttribute(FilmoviController.POSECENI_FILMOVI_ZA_KORISNIKA_KEY, poseceniFilmovi);
////		
		System.out.println("Success!");
	}
	
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("Destroying session...");
		
		System.out.println("Success!");
	}

}
