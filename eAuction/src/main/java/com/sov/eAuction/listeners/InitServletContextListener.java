package com.sov.eAuction.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.stereotype.Component;

@Component
public class InitServletContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent event)  {
    	System.out.println("Updating context...");
    	
//    	servletContext.setAttribute(FilmoviController.FILMOVI_KEY, new Filmovi());	
//		servletContext.setAttribute(FilmoviController.STATISTIKA_FILMOVA_KEY, new FilmStatistika());
    	
    	System.out.println("Success!");
    }
    
    /** kod koji se izvrsava po nakon uništavanja ServletContext objekta */
	public void contextDestroyed(ServletContextEvent event)  { 
    	System.out.println("Deleting context...");
    		
    	System.out.println("Success!");
    }
}
